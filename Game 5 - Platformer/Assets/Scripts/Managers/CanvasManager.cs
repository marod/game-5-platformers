﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CanvasManager : MonoBehaviour
{

    public TextMeshProUGUI ScoreText;

    public SpriteRenderer Key1, Key2, Key3;
    public List<Sprite> sprites;

    public void UpdateScoreText(int number)
    {
        ScoreText.text = number.ToString();
    }

    public void UpdateKeys(int keyNumber)
    {
        switch (keyNumber)
        {
            case 1:
                Key1.sprite = sprites[0];
                break;
            case 2:
                Key2.sprite = sprites[1];
                break;
            case 3:
                Key3.sprite = sprites[2];
                break;
        }
    }
}
