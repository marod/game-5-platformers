﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public GameObject BlueEntrance, GreenEntrance, YellowEntrance;

    private int scoreBoxes;

    public int ScoreBoxes
    {
        get
        {
            return scoreBoxes;
        }

        set
        {
            scoreBoxes = value;
            Managers.Canvas.UpdateScoreText(value);
        }
    }

    public int KeysCollected
    {
        get
        {
            return keysCollected;
        }

        set
        {
            keysCollected = value;
        }
    }

    private int keysCollected;

    public void UpdateKeys(int keyNumber)
    {
        switch (keyNumber)
        {
            case 1:
                Debug.Log("Entra");
                BlueEntrance.SetActive(false);
                break;
            case 2:
                GreenEntrance.SetActive(false);
                break;
            case 3:
                YellowEntrance.SetActive(false);
                break;
        }

        KeysCollected += 1;
    }
}
