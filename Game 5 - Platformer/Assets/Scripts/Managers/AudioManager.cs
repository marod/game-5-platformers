﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public AudioSource musicSource;
	public AudioSource efxSource;

	public void PlaySingle(AudioClip clip){
		efxSource.clip = clip;
		efxSource.PlayOneShot(clip);
	}

}
