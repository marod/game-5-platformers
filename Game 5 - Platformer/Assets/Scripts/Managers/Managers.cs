﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Managers : MonoBehaviour {

	private static CanvasManager _canvas;

    public static CanvasManager Canvas
    {
        get
        {
            return _canvas;
        }
    }

	
	private static GameManager _game;

    public static GameManager Game
    {
        get
        {
            return _game;
        }
    }

    
	private static AudioManager _music;

    public static AudioManager Music
    {
        get
        {
            return _music;
        }
    }

    void Awake()
	{
		_canvas = this.GetComponent<CanvasManager>();
		_game = this.GetComponent<GameManager>();
        _music = this.GetComponent<AudioManager>();
	}
}
