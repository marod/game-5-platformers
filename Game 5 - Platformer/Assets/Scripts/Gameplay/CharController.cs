﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharController : MonoBehaviour
{


    Rigidbody2D rigidbody;
    Animator animator;
    SpriteRenderer spriteRenderer;

    Vector3 initialPosition;

    public AudioClip deathClip;
    public AudioClip jumpClip;
    public AudioClip landingClip;
    public ParticleSystem explosionParticle;
    public float movementSpeed;
    public float jumpForce;

    public GameObject uiPanel;

    public float fallMultiplier;

    float currentGravity;

    public float lowJumpMultiplier;

    public bool canJump;
    bool isGrounded;

    bool isWallJumping;

    bool isDucking;

    bool droped;

    bool facingRight;
    bool facintLeft;

    public Transform groundCheck;
    public float groundRadius;

    public Transform wallCheck;
    public Vector2 sizeCheck;
    public LayerMask whatIsGround;
    public LayerMask whatIsWall;

    public GameObject platformDroped;

    public bool IsGrounded
    {
        get
        {
            return isGrounded;
        }

        set
        {
            var beforeValue = isGrounded;
            isGrounded = value;
            if (isGrounded == true & beforeValue == false)
            {
                Managers.Music.PlaySingle(landingClip);
                explosionParticle.Play();
            }

        }
    }

    void Start()
    {
        initialPosition = transform.position;
        rigidbody = this.GetComponent<Rigidbody2D>();
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        animator = this.GetComponent<Animator>();
        isGrounded = true;
    }

    // Update is called once per frame
    void Update()
    {

        if(uiPanel.activeSelf){
            if(Input.GetKeyDown(KeyCode.E) && Managers.Game.KeysCollected == 3){
                SceneManager.LoadScene("Ending");
            }
        }
        Movement();
    }

    /// <summary>
    /// Callback to draw gizmos that are pickable and always drawn.
    /// </summary>
    void OnDrawGizmos()
    {
        Gizmos.DrawCube(wallCheck.position, sizeCheck);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            Managers.Music.PlaySingle(deathClip);
            transform.position = initialPosition;
        }

        if (other.gameObject.GetComponent<DoorController>())
        {
            if(!uiPanel.activeSelf && Managers.Game.KeysCollected == 3){
                uiPanel.SetActive(true);
            }
        }

    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DoorController>())
        {
            if(uiPanel.activeSelf){
                uiPanel.SetActive(false);
            }
        }
    }

    void Movement()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        if (!isDucking && !isWallJumping)
        {
            HorizontalMovement(horizontal);
        }

        if(isWallJumping && isGrounded){
            HorizontalMovement(horizontal);
        }

        if(!isGrounded && !isWallJumping && rigidbody.velocity.y <= 0){
            rigidbody.gravityScale = 2.8f;
        } else {
            rigidbody.gravityScale = 2;
        }

        IsGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        animator.SetBool("isGrounded", IsGrounded);

        if (horizontal > 0 && !isWallJumping)
        {
            spriteRenderer.flipX = false;
            facingRight = true;
            facintLeft = false;
        }
        else if (horizontal < 0 && !isWallJumping)
        {
            spriteRenderer.flipX = true;
            facingRight = false;
            facintLeft = true;
        }

        isWallJumping = Physics2D.OverlapBox(wallCheck.position, sizeCheck, 0, whatIsWall);

        if (isWallJumping && !isGrounded)
        {
            //Vector2 move = new Vector2(0, rigidbody.velocity.y);
            if (!isGrounded && rigidbody.velocity.y <= 0)
            {
                Vector2 move = new Vector2(rigidbody.velocity.x, rigidbody.velocity.y);
                if (Input.GetKey(KeyCode.RightArrow) && facintLeft || Input.GetKey(KeyCode.LeftArrow) && facingRight)
                {
                    move.x = horizontal * movementSpeed;
                    animator.SetFloat("Speed", Mathf.Abs(move.x));
                }
                move.y = -1f;


                rigidbody.velocity = move;
            }
        }

        animator.SetBool("isWallJump", isWallJumping);

        if (Input.GetKey(KeyCode.DownArrow) && IsGrounded)
        {
            isDucking = true;
        }
        else
        {
            isDucking = false;
        }

        if (droped && isGrounded)
        {
            droped = false;
            canJump = true;
            platformDroped.GetComponent<Collider2D>().enabled = true;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (Input.GetButtonDown("Jump") && Physics2D.IsTouchingLayers(GetComponent<Collider2D>(), LayerMask.GetMask("Platform")))
            {
                canJump = false;
                Collider2D collider = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
                droped = true;
                collider.enabled = false;
                platformDroped = collider.gameObject;

            }
        }

        if (canJump)
        {
            if (isWallJumping && Input.GetButtonDown("Jump"))
            {
                Managers.Music.PlaySingle(jumpClip);

                if (facingRight && horizontal <= 0 || facintLeft && horizontal >= 0)
                {
                    
                    if (facingRight)
                    {
                        HorizontalMovement(-1);
                    }
                    else
                    {
                        HorizontalMovement(1);
                    }

                    rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

                }


            }

            if (Input.GetButtonDown("Jump") && IsGrounded)
            {
                Managers.Music.PlaySingle(jumpClip);
                rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            }
        }

        animator.SetBool("isDucking", isDucking);
        animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));
    }

    void HorizontalMovement(float horizontalAxis)
    {
        Vector2 move = new Vector2(0, rigidbody.velocity.y);
        move.x = horizontalAxis * movementSpeed;
        rigidbody.velocity = move;
    }

}
