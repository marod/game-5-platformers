﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour {

	void OnTriggerStay2D(Collider2D other)
	{
		if(other.gameObject.GetComponent<CharController>() && Input.GetKey(KeyCode.E)){
			SceneManager.LoadScene("Gameplay");
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.GetComponent<CharController>()){
			var canvas = other.gameObject.GetComponent<CharController>().uiPanel;
			canvas.SetActive(true);
		}
	}

		void OnTriggerExit2D(Collider2D other)
	{
		if(other.gameObject.GetComponent<CharController>()){
			var canvas = other.gameObject.GetComponent<CharController>().uiPanel;
			canvas.SetActive(false);
		}
	}
}
