﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsmeraldController : MonoBehaviour {

	List<Sprite> esmeraldSprites;

	void OnEnable(){
		SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer>();
		var pos = Random.Range(0, esmeraldSprites.Count);
		spriteRenderer.sprite = esmeraldSprites[pos];
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if(other.gameObject.GetComponent<CharacterController>()){
			Destroy(this.gameObject);
		}
	}
}
