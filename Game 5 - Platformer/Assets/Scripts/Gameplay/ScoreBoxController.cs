﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoxController : MonoBehaviour {

	public List<Sprite> ScoreBoxSprites;

	public AudioClip clip;

	void OnEnable(){
		SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer>();
		var pos = Random.Range(0, ScoreBoxSprites.Count);
		spriteRenderer.sprite = ScoreBoxSprites[pos];
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this
	/// object (2D physics only).
	/// </summary>
	/// <param name="other">The other Collider2D involved in this collision.</param>
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.GetComponent<CharController>()){
			Managers.Music.PlaySingle(clip);
			this.gameObject.SetActive(false);
			Managers.Game.ScoreBoxes += 1;
			
		}
	}
}
