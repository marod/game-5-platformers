﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour {

	public int keyNumber;

	public AudioClip clip;
		void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.GetComponent<CharController>()){
			Managers.Music.PlaySingle(clip);
			this.gameObject.SetActive(false);
			Managers.Canvas.UpdateKeys(keyNumber);
			Managers.Game.UpdateKeys(keyNumber);
		}
	}
}
