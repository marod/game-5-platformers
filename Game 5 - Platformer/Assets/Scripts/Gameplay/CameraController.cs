﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject Target;

    public float lerpSpeed;

    public float downVision;

    public float upVision;

    private Rigidbody2D targetRigidbody;

    Camera camera;

    // Use this for initialization
    void Start()
    {
        camera = this.GetComponent<Camera>();
        targetRigidbody = Target.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void LateUpdate()
    {

        var lerpTime = lerpSpeed * Time.deltaTime;
        Vector3 pos;
        if (Input.GetKey(KeyCode.DownArrow) && targetRigidbody.velocity.magnitude == 0)
        {
            pos = Vector2.Lerp(camera.transform.position, new Vector3(Target.transform.position.x, Target.transform.position.y - downVision), lerpTime);
        }
        else
        {
			if(targetRigidbody.velocity.y < -10){
				lerpSpeed = 2f;
				pos = Vector2.Lerp(camera.transform.position, new Vector3(Target.transform.position.x, Target.transform.position.y - downVision), lerpTime);
			} else {
				lerpSpeed = 1f;
				pos = Vector2.Lerp(camera.transform.position, new Vector3(Target.transform.position.x, Target.transform.position.y + upVision), lerpTime);
			}
        }
        camera.transform.position = new Vector3(pos.x, pos.y, camera.transform.position.z);
    }
}
