﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ChangeWaterDirection : MonoBehaviour
{

    float currentRotationTime;

    float rotationTime = 1f;

    Tilemap tilemap;
    void Awake()
    {
        tilemap = this.GetComponent<Tilemap>();
    }

    void Update()
    {
        if (Time.time - currentRotationTime >= rotationTime)
        {
			Quaternion newRotation;
            Matrix4x4 matrix;

            // if (tileRenderer.)
            // {
                
        		
            // }
            // else
            // {
            //     newRotation = Quaternion.Euler(0f, 180f, 0f);
			// 	matrix = Matrix4x4.TRS(Vector3.zero, newRotation, Vector3.one);
            // }
			Debug.Log(tilemap.orientationMatrix);
			matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 90f, 0f), new Vector3(-11f, 1f, 1f));
			tilemap.SetTransformMatrix(new Vector3Int(0, 0, 0), matrix);
            currentRotationTime = Time.time;
        }
    }

}
